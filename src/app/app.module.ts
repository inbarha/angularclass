import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

//firebase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';


//angular material
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { CodesComponent } from './codes/codes.component';
import { Routes, RouterModule} from '@angular/router';


import {environment} from '../environments/environment';
import { UsertodosComponent } from './usertodos/usertodos.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    TodosComponent,
    TodoComponent,
    RegistrationComponent,
    LoginComponent,
    CodesComponent,
    UsertodosComponent
  ],
  imports: [
    MatCardModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    BrowserAnimationsModule,

    RouterModule.forRoot([

      {path:'',component:TodosComponent}, //היו אר אל ריק כי אלה דפים משתנים
      {path:'register',component:RegistrationComponent},
      {path:'login',component:LoginComponent},
      {path:'codes',component:CodesComponent},
      {path:'usertodos',component:UsertodosComponent},
      {path:'**',component:TodosComponent}//לא לשים אחריו את ראוט,זה לא יעבוד
      
      
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

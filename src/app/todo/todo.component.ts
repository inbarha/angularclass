import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { flush } from '@angular/core/testing';
import { TodosService } from '../todos.service';


@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})


export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();
  
  text;
  id;
  key;
  tempText;
 
  showTheButton=false;
  showEditField = false;

  delete(){
    this.TodosService.delete(this.key)
  }
   
  send(){
    console.log('event caught')
    this.myButtonClicked.emit(this.text);
  }

  showButton(){
    this.showTheButton = true;

  }
  hideButton(){
    this.showTheButton = false;

  }

  showEdit(){
    this.showEditField=true;
    this.tempText = this.text;
  }

  cancel(){
    this.showEditField=false;
    this.text = this.tempText;
  }
  save(){
    this.TodosService.update(this.key, this.text)
    this.showEditField=false;
  }
  constructor(private TodosService:TodosService) { }
  ngOnInit() {
    this.text = this.data.text;
    this.id = this.data.id;
    this.key = this.data.$key;

  }

}

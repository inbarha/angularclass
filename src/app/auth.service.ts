import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {Observable} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  login(email:string,password:string){
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email,password)
  }

  
logout(){
  return this.fireBaseAuth.auth.signOut();
}

  
  signup(email:string,password:string){
   return this.fireBaseAuth
    .auth
    .createUserWithEmailAndPassword(email,password);
  }
  addUser(user, name: string){
      let uid = user.uid;
      let ref =this.db.database.ref('/');
      ref.child('users').child(uid).push({'name':name});

  }

    user:Observable<firebase.User>;
updateProfile(user,name:string){
  user.updateProfile({displayName:name,photoURL:''});
}
  constructor(private fireBaseAuth:AngularFireAuth, private db:AngularFireDatabase) { 
    this.user = fireBaseAuth.authState;
  }
}

import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database'
import { AuthService } from '../auth.service';
import { TodosService } from '../todos.service';


@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  todoTextFromTodo="no text"

  showText($event){
    this.todoTextFromTodo = $event
  }

  todos = [];
  text:string;

addTodo(){
  this.todosService.addTodo(this.text)
  this.text = '';
}

  constructor(private db:AngularFireDatabase, 
    private authService:AuthService,
    private todosService:TodosService) { }

  ngOnInit() {

      this.authService.user.subscribe(user => {
        this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
          todos => {
            this.todos = [];
              todos.forEach(
                todo =>{
                  let y =todo.payload.toJSON();
                  y["$key"] = todo.key;
                  this.todos.push(y);
                }
              )

          }
        ) 
      })
  }

}

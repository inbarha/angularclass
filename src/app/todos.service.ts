import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class TodosService {

  delete(key){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+ user.uid+'/todos').remove(key);
    })
  }

  update(key,text){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+ user.uid+'/todos').update(key,{'text':text})
    })
  
  }
  addTodo(text:string){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+ user.uid+'/todos').push({'text':text});
    })
  }

  constructor(private authService:AuthService,
              private db:AngularFireDatabase,
            ) { }
}

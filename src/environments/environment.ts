// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBqNdwGuHTTOCqBr-R5NgatHG7d4x7s7Yc",
    authDomain: "todos7-ec667.firebaseapp.com",
    databaseURL: "https://todos7-ec667.firebaseio.com",
    projectId: "todos7-ec667",
    storageBucket: "todos7-ec667.appspot.com",
    messagingSenderId: "802933431763"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
